-- [SECTION] CRUD Operations

-- Create Records

-- To insert an artists in the artists table:
INSERT INTO artists(name) VALUES ("Blackpink");
INSERT INTO artists(name) VALUES ("Rivermaya");

-- To insert albums in the albums table:
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("The Album", "2020-10-02", 1);
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Trip", "1996-01-01", 2);

ALTER TABLE albums CHANGE COLUMN data_released date_released DATE NOT NULL;

-- To insert songs in the songs table:
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Ice Cream", "00:04:16", "Kpop", 1);
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("You Never Know", "00:03:59", "Kpop", 1);
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Kundiman", "00:03:54", "OPM", 2);
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Kisapmata", "00:04:39", "OPM", 2);

-- [SECTION] Read/Select

-- Display the title and genre of all the songs.
SELECT song_name, genre FROM songs;

-- Display the title of all the fields in songs table.
SELECT * FROM songs;

-- Display the title of all the OPM songs.
SELECT song_name FROM songs WHERE genre = "OPM";

-- Display the title and length of the Kpop songs that are more than 4:00
SELECT song_name, length FROM songs WHERE length > "00:04:00" AND genre = "Kpop";

-- [SECTION] Update Records

-- Update the length of You Never Know to 4:00
UPDATE songs SET length = "00:04:00" WHERE song_name = "You Never Know";

-- Removing the WHERE clause will update all rows/records.
UPDATE songs SET length = "00:04:00";

-- [SECTION] Deleting Records

-- Delete all Kpop songs that are more than 4:00.
DELETE FROM songs WHERE genre = "Kpop" AND length > "00:04:00";

-- Removing the WHERE clause will delte all the rows/records;
DELETE FROM songs;  